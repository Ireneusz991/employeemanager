package com.gatearrays.jaxb;

import java.io.File;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class TestCarMarshing
{


    static Cars cars = new Cars();
    static
    {
        cars.setCars(new ArrayList<Car>());

        Car car = new Car();
        car.setId(1);
        car.setColor("red");
        car.setType("fast");

        Car car1 = new Car();
        car1.setId(2);
        car1.setColor("blue");
        car1.setType("Sedan");

        Car car2 = new Car();
        car2.setId(3);
        car2.setColor("White");
        car2.setType("hatchback");


        cars.getCars().add(car);
        cars.getCars().add(car1);
        cars.getCars().add(car2);
    }

    public static void main(String[] args) throws JAXBException
    {
        marshalingExample();
    }

    private static void marshalingExample() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Cars.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(cars, System.out);
        jaxbMarshaller.marshal(cars, new File("cars.xml"));
    }
}
