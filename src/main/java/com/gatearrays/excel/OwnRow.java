package com.gatearrays.excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class OwnRow {
    private String val1;
    private String val2;
    private String val3;
    private String val4;
}
